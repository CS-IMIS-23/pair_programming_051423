package ArithMathOperation;
//*************************************************************************************
//    NifixExpression.java        Author:谭鑫
//
//    生成中缀式子的类，继承了character类进行调用运算符，以及用brackets进行加括号。
//*************************************************************************************
import java.util.Random;

public class NifixExpression extends Character
{
    String result = "";
    Random generator = new Random();

    public String Level(int num,int sum)//num 等级 sum 确定分式整式
    {
        if (num == 1&&sum == 1)//生成两个整数的四则运算
        {
            int a = generator.nextInt(100);
            int b = generator.nextInt(100);
            result = a + super.character() + b + " = ";
        }
        else if (num != 1&&sum == 1)//生成多于两个整数的四则运算
        {
            for (int e = 0; e < num - 1 ; e++)//for循环进行生成一个数和一个运算符
            {
                int d = generator.nextInt(100);
                result += d + super.character();
            }
            int f = generator.nextInt(100);//补一个数进行完整的式子产生
            result += f + " = ";
            result = Brackets.Brackets(result,num);
        }
        else
            {
                if (num == 1&&sum == 2)////生成两个分数的四则运算
                {
                    int o = generator.nextInt(99) + 1;
                    int p = generator.nextInt(99) + 1;
                    int q = generator.nextInt(99) + 1;
                    int r = generator.nextInt(99) + 1;
                    result = new RationalNumbers(o, p) + super.character() + new RationalNumbers(q,r) + " = ";
                }
                else
                    {
                        if (num != 1&&sum == 2)//生成多于两个分数的四则运算
                        {
                            for(int g = 0;g < num - 1 ;g++)//for循环进行生成一个分数和一个运算符
                            {
                                int h = generator.nextInt(99) + 1;
                                int i = generator.nextInt(99) + 1;
                                result += new RationalNumbers(h,i) + super.character();
                            }
                            int j = generator.nextInt(98) + 2;
                            int k = generator.nextInt(98) + 2;
                            result += new RationalNumbers(j,k) + " = ";//补一个分数进行完整的式子产生
                            result = Brackets.Brackets(result,num);
                        }
                        else
                            {
                                if (num ==1&&sum == 3)//分数和整数的加减，生成一个分数和整数的四则运算
                                {
                                    int s = generator.nextInt(99) + 1;
                                    int t = generator.nextInt(99) + 1;
                                    int u = generator.nextInt(100);
                                    result = new RationalNumbers(s, t) + super.character() + u + " = ";
                                }
                                else if (num != 1&&sum == 3)//分数和整数的加减，生成多于两个整数和分数的四则运算
                                {
                                    if(num % 2 == 0)//判断num为偶数
                                    {
                                        for(int z = 1;z < num ;z++)//for循环进行生成一个数和一个运算符
                                        {
                                            int v = generator.nextInt(100);
                                            int w = generator.nextInt(100);
                                            int l = generator.nextInt(100);
                                            result += new RationalNumbers(v,w) + super.character();
                                            if(z == (num / 2))//在第z = num / 2 的的时候进行补一个分数，然后中断循环
                                            {
                                                int f = generator.nextInt(100);
                                                int y = generator.nextInt(100);
                                                result += new RationalNumbers(f,y) + " = ";
                                                Brackets.Brackets(result,num);
                                                break;
                                            }
                                            result += l + super.character();
                                        }
                                    }
                                    else if(num % 2 != 0)//判断num为奇数
                                    {
                                        for(int z = 0;z < num / 2 ;z++)//for循环进行生成一个数和一个运算符
                                        {
                                            int v = generator.nextInt(100);
                                            int w = generator.nextInt(100);
                                            int l = generator.nextInt(100);
                                            result += new RationalNumbers(v,w) + super.character();
                                            result += l + super.character();
                                        }
                                        int x = generator.nextInt(100);
                                        int y = generator.nextInt(100);
                                        result += new RationalNumbers(x,y) + " = ";//补一个分数进行完整的式子产生
                                        result = Brackets.Brackets(result,num);
                                    }
                                }
                            }
                    }
            }
        return result;
    }
    public String Level(String num)
    {
        Random generator = new Random();
        int sum = generator.nextInt(5) + 1;//随机生成在1-5之间的随机数
        if (num.equals("+"))//加法运算
        {
            for(int z = 0;z < sum;z++)//for循环进行生成一个整数、一个运算符和一个分数
            {
                int v = generator.nextInt(100);
                int w = generator.nextInt(100);
                int l = generator.nextInt(100);
                result += new RationalNumbers(v,w) + super.character(1);
                result += l + super.character(1);
            }
            int x = generator.nextInt(100);
            int y = generator.nextInt(100);
            result += new RationalNumbers(x,y) + " = ";//补一个分数进行完整的式子产生

        }
        else
            {
            if (num.equals("-"))//减法运算
            {
                for (int a = 0; a < sum; a++)//for循环进行生成一个数和一个运算符
                {
                    int v = generator.nextInt(100);
                    int w = generator.nextInt(100);
                    int l = generator.nextInt(100);
                    result += new RationalNumbers(v,w) + super.character(2);
                    result += l + super.character(2);
                }
                int x = generator.nextInt(100);
                int y = generator.nextInt(100);
                result += new RationalNumbers(x,y) + " = ";//补一个分数进行完整的式子产生
            }
            else
                {
                if (num.equals("*"))//乘法运算
                {
                    for (int a = 0; a < sum; a++)//for循环进行生成一个数和一个运算符
                    {
                        int v = generator.nextInt(100);
                        int w = generator.nextInt(100);
                        int l = generator.nextInt(100);
                        result += new RationalNumbers(v,w) + super.character(3);
                        result += l + super.character(3);
                    }
                    int x = generator.nextInt(100);
                    int y = generator.nextInt(100);
                    result += new RationalNumbers(x,y) + " = ";//补一个分数进行完整的式子产生
                }
                else
                    {
                        if(num.equals("÷"))//除法运算
                        {
                            for (int a = 0; a < sum; a++)//for循环进行生成一个数和一个运算符
                            {
                                int v = generator.nextInt(100);
                                int w = generator.nextInt(100);
                                int l = generator.nextInt(100);
                                result += new RationalNumbers(v,w) + super.character(4);
                                result += l + super.character(4);
                            }
                            int x = generator.nextInt(100);
                            int y = generator.nextInt(100);
                            result += new RationalNumbers(x,y) + " = ";//补一个分数进行完整的式子产生
                        }
                    }
                }
            }
        return result;
    }
}