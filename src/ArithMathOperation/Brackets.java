package ArithMathOperation;
//*************************************************************************************
//    Brackets.java        Author:谭鑫
//
//    对中缀的式子进行添括号
//*************************************************************************************
import java.util.Random;

public class Brackets
{
    public static String Brackets(String stance1, int num)
    {
        Random generator = new Random();
        String b, c = "";
        int a = 0, e, d;
        int u = generator.nextInt(2);

        if (num >= 10)
        {
            if (u == 1)//进行判断是否添加括号
            {
                while (a <= stance1.length()) {
                    if (("*".equals(String.valueOf((stance1.charAt(a))))) || "÷".equals((String.valueOf((stance1.charAt(a))))))//对第一次出现的“÷”，“*”开始进行添加括号
                    {
                        if (a + 5 < stance1.length())//判断第一次出现“÷”，“*”距离“=”之间是否小于5，判断是否进行添加括号
                        {
                            e = stance1.length() - a;
                            d = generator.nextInt(e - 15) + 10;//随机进行产生加括号的长度
                            if (" ".equals(String.valueOf(stance1.charAt(a + d))))//判断a + d 的索引处是否为空字符串
                            {
                                //判断a + d - 1 的索引处是否为运算符
                                if (("+".equals(String.valueOf(stance1.charAt(a + d - 1)))) || ("-".equals(String.valueOf(stance1.charAt(a + d - 1)))) || ("*".equals(String.valueOf(stance1.charAt(a + d - 1)))) || ("÷".equals(String.valueOf(stance1.charAt(a + d - 1))))) {
                                    b = (String) stance1.subSequence(a + 1, a + d - 2);
                                    if ((String.valueOf(b.charAt(b.length() - 1)).equals(" ")))//扣去产生的多余括号
                                        b = (String) b.subSequence(0, b.length() - 1);
                                    c = stance1.subSequence(0, a + 1) + " (" + b.concat(" )") + stance1.subSequence(a + d - 2, (stance1.length()));//进行拼接
                                } else//如果a + d - 1 的索引处不是运算符，那么为数字
                                {
                                    b = (String) stance1.subSequence(a + 1, a + d);
                                    if ((String.valueOf(b.charAt(b.length() - 1)).equals(" ")))//扣去产生的多余括号
                                        b = (String) b.subSequence(0, b.length() - 1);
                                    c = stance1.subSequence(0, a + 1) + " (" + b.concat(" )") + stance1.subSequence(a + d, (stance1.length()));//进行拼接
                                }
                            } else {
                                if (("+".equals(String.valueOf(stance1.charAt(a + d)))) || ("-".equals(String.valueOf(stance1.charAt(a + d)))) || ("*".equals(String.valueOf(stance1.charAt(a + d)))) || ("÷".equals(String.valueOf(stance1.charAt(a + d)))))//判断a + d 的索引处是否为运算符
                                {
                                    b = (String) stance1.subSequence(a + 1, a + d - 1);
                                    if ((String.valueOf(b.charAt(b.length() - 1)).equals(" ")))//扣去产生的多余括号
                                        b = (String) b.subSequence(0, b.length() - 1);
                                    c = stance1.subSequence(0, a + 1) + " (" + b.concat(" )") + stance1.subSequence(a + d - 1, (stance1.length()));//进行拼接
                                } else//判断a + d 的索引处是否为数字或者是分数线
                                {
                                    //循环扫描直至遇到的索引位置出为空字符串
                                    while (("0".equals(stance1.charAt(a + d))) || ("1".equals(String.valueOf(stance1.charAt(a + d)))) || ("2".equals(String.valueOf(stance1.charAt(a + d)))) || ("3".equals(String.valueOf(stance1.charAt(a + d)))) || ("4".equals(String.valueOf(stance1.charAt(a + d)))) || ("5".equals(String.valueOf(stance1.charAt(a + d)))) || ("6".equals(String.valueOf(stance1.charAt(a + d)))) || ("7".equals(String.valueOf(stance1.charAt(a + d)))) || ("8".equals(String.valueOf(stance1.charAt(a + d)))) || ("9".equals(String.valueOf(stance1.charAt(a + d)))) || ("/".equals(String.valueOf(stance1.charAt(a + d))))) {
                                        d++;
                                    }
                                    b = (String) stance1.subSequence(a + 1, a + d + 1);
                                    if ((String.valueOf(b.charAt(b.length() - 1)).equals(" ")))//对产生的括号问题进行判断，然后进行添加括号和拼接
                                    {
                                        b = (String) b.subSequence(0, b.length() - 1);
                                        c = stance1.subSequence(0, a + 1) + " (" + b.concat(" ) ") + stance1.subSequence(a + d + 1, (stance1.length()));
                                    } else
                                        c = stance1.subSequence(0, a + 1) + " (" + b.concat(" )") + stance1.subSequence(a + d + 1, (stance1.length()));
                                }
                            }
                        }
                        break;
                    } else
                        a++;
                }
                return c;
            } else
                return stance1;
        } else
            return stance1;
    }
}
