package ArithMathOperation;
//*************************************************************************************
//    Character.java        Author:谭鑫
//
//    生成运算符的类。
//*************************************************************************************
import java.util.Random;

public class Character
{
    Random generator = new Random();
    protected String[] chars= {"+","-","*","÷"};
    public String character()//方法重载，随机生成运算符
    {
        int num = generator.nextInt(4);
        String result = " " + chars[num] + " ";
        return result;
    }
    public String character(int a)//方法重载，有目的的生成运算符
    {
        String result = " " + chars[a - 1] + " ";
        return result;
    }
}
