package ArithMathOperation;
//*************************************************************************************
//    Test.java        Author:谭鑫
//
//    把个各类进行组合的产品代码，以及写入文件。
//*************************************************************************************
import java.io.*;
import java.text.NumberFormat;
import java.util.Scanner;

public class Test
{
    public static void main(String[] args) throws IOException
    {
        NumberFormat fmt = NumberFormat.getPercentInstance();
        Scanner scan = new Scanner(System.in);
        Logion logion = new Logion();

        int right = 0;
        int wrong = 0;
        double rate;

        System.out.print("请输入您的名字：");
        String name = scan.nextLine();
        System.out.print("请输入您的学号：");
        String number = scan.nextLine();
        System.out.print("混合运算为A，简便运算为B，请选择：");
        String z = scan.nextLine();

        FileWriter file = new FileWriter(name + ".txt");
        file.write("姓名：" + name +"\n学号：" + number);

        //进行混合运算
        if(z.equalsIgnoreCase("A"))
        {
            //生成题目
            System.out.print("整数运算为1，分式运算为2，整分运算为3，请选择：");
            int a = scan.nextInt();
            System.out.print("请输入您要做的题目等级: ");
            int b = scan.nextInt();
            System.out.print("请输出该等级的题目量: ");
            int c = scan.nextInt();
            System.out.println();

            for (int d = 1; d <= c; d++)
            {
                NifixExpression num1 = new NifixExpression();
                SuffixExpression num2 = new SuffixExpression();
                Judge num3 = new Judge();
                String num = num1.Level(b, a);
                System.out.print("问题" + d + ": " + num + " ");
                String f = num3.answer(num2.SuffixExpression(num));
                String e = scan.next();
                if(f.equals(e))
                {
                    System.out.println("回答正确！\n");
                    right++;
                    file.write("\n问题" + d + ": " + num + e +"   答案正确！\n");
                }
                else
                    {
                        System.out.println("回答错误！\t"  + "\t正确答案：" + f + "\n" + logion.Wisdom() + "\n");
                        wrong++;
                        file.write("\n问题" + d + ": " + num + e +"   答案错误！\t正确答案：" + f + "\n");
                    }
            }
        }
        //进行简便运算
        else if(z.equalsIgnoreCase("B"))
        {
            System.out.print("加法运算为\"+\"，减法运算为\"-\"，乘法运算为\"*\"，除法运算为\"÷\",请选择：");
            String a = scan.next();
            if (a.equals("+") || a.equals("-") || a.equals("*") || a.equals("÷"))
            {
                System.out.print("请输出该等级的题目量: ");
                int c = scan.nextInt();
                System.out.println();

                for (int d = 1; d <= c; d++)
                {
                    NifixExpression num1 = new NifixExpression();
                    SuffixExpression num2 = new SuffixExpression();
                    Judge num3 = new Judge();
                    String num = num1.Level(a);
                    System.out.print("题目" + d + ": " + num + " ");
                    String f = num3.answer(num2.SuffixExpression(num));
                    String e = scan.next();
                    if(f.equals(e))
                    {
                        System.out.println("回答正确！\n");
                        right++;
                        file.write("\n问题" + d + ": " + num + e +"   答案正确！\n");
                    }
                    else
                    {
                        System.out.println("回答错误！\t" + "正确答案：" + f + "\n" + logion.Wisdom() + "\n");
                        wrong++;
                        file.write("\n问题" + d + ": " + num + e +"   答案错误！\t正确答案：" + f + "\n");
                    }
                }
            }
            else
                System.out.println("请输入'+','-'.'*','÷'任意一个！！！");
        }

            //进行判断正误
            System.out.println("答对题目量：" + right + "\n答错题目量：" + wrong);
            rate = (double) right / (right + wrong);
            System.out.println("正确率：" + fmt.format(rate));

            //写入文件
            file.write("\n答对题目量：" + right + "\n答错题目量：" + wrong + "\n正确率为" + fmt.format(rate));
            file.flush();
        }
}


